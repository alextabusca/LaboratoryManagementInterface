package model;

import java.sql.Date;

public class Assignment {

    private Long id;
    private String title;
    private Date deadline;
    private String description;
    private Laboratory laboratory;

    public Assignment() {

    }

    public Assignment(Long id, String title, Date deadline, String description, Laboratory laboratory) {
        this.id = id;
        this.title = title;
        this.deadline = deadline;
        this.description = description;
        this.laboratory = laboratory;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Laboratory getLaboratoryDTO() {
        return laboratory;
    }

    public void setLaboratoryDTO(Laboratory laboratory) {
        this.laboratory = laboratory;
    }

    /*public String toString() {
        return "Assignment{assignmentID='" + id + '\'' + ", title='" + title + '\'' + ", deadline='" + deadline + '\'' + ", description='" + description + '}' + '\n';
    }*/

    public String toString() {
        return id.toString();
    }
}
