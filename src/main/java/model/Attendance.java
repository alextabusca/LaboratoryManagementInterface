package model;

public class Attendance {

    private Long id;
    private Student student;
    private Laboratory laboratory;
    private boolean isPresent;

    public Attendance() {

    }

    /*public Attendance(Student student, Laboratory laboratory) {
        this.student = student;
        this.laboratory = laboratory;
    }*/

    public Attendance(Long id, Student student, Laboratory laboratory, boolean isPresent) {
        this.id = id;
        this.student = student;
        this.laboratory = laboratory;
        this.isPresent = isPresent;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isPresent() {
        return isPresent;
    }

    public void setPresent(boolean present) {
        isPresent = present;
    }

    public Student getStudentDTO() {
        return student;
    }

    public void setStudentDTO(Student student) {
        this.student = student;
    }

    public Laboratory getLaboratoryDTO() {
        return laboratory;
    }

    public void setLaboratoryDTO(Laboratory laboratory) {
        this.laboratory = laboratory;
    }
}
