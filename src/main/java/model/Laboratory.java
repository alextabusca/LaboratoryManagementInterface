package model;

import java.sql.Date;

public class Laboratory {

    private Long id;
    private String title;
    private Date date;
    private String mainTopics;
    private String longDescription;

    public Laboratory() {

    }

    public Laboratory(Long id, String title, Date date, String mainTopics, String longDescription) {
        this.id = id;
        this.title = title;
        this.date = date;
        this.mainTopics = mainTopics;
        this.longDescription = longDescription;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMainTopics() {
        return mainTopics;
    }

    public void setMainTopics(String mainTopics) {
        this.mainTopics = mainTopics;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    /*public String toString() {
        return "Laboratory{laboratoryID='" + id + '\'' + ", title='" + title + '\'' + ", mainTopics='" + mainTopics + '\'' + ", longDescription='" + longDescription + '\'' + ", date='" + date + '\'' + '}' + '\n';
    }*/

    public String toString() {
        return id.toString();
    }
}
