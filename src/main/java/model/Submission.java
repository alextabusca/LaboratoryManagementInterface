package model;

import java.sql.Date;

public class Submission {

    private Long id;
    private Date date;
    private String description;
    private int grade;

    private Assignment assignment;
    private Student student;

    public Submission() {

    }

    public Submission(Long id, Student student, Assignment assignment, Date date, String description, int grade) {
        this.id = id;
        this.student = student;
        this.assignment = assignment;
        this.date = date;
        this.description = description;
        this.grade = grade;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public Assignment getAssignmentDTO() {
        return assignment;
    }

    public void setAssignmentDTO(Assignment assignment) {
        this.assignment = assignment;
    }

    public Student getStudentDTO() {
        return student;
    }

    public void setStudentDTO(Student student) {
        this.student = student;
    }
}
