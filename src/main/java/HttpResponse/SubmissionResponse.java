package HttpResponse;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.Submission;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Date;
import java.util.List;

public class SubmissionResponse {

    private ObjectMapper objectMapper = new ObjectMapper();

    public SubmissionResponse() {

    }

    public void getAllSubmissions() {
        List<Submission> submissionList;

        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();

            HttpGet request = new HttpGet("http://localhost:8080/submissions/getAllSubmissions");
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String result;
            result = rd.readLine();

            submissionList = objectMapper.readValue(result, new TypeReference<List<Submission>>() {
            });

            System.out.println(submissionList);

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
        }
    }

    public String getSubmissionByID(Long submissionID) {
        new Submission();
        Submission submission;

        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();

            String requestString = "http://localhost:8080/submissions/%7BsubmissionID%7D?submissionID=" + submissionID;
            HttpGet request = new HttpGet(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String result;
            result = rd.readLine();

            submission = objectMapper.readValue(result, Submission.class);
            System.out.println(submission.toString());

            return result;

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void gradeSubmission(Long submissionID, int grade) {
        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();

            String requestString = "http://localhost:8080/submissions/%7BsubmissionID%7D/%7Bgrade%7D?submissionID=" + submissionID + "&grade=" + grade;

            HttpPut request = new HttpPut(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            System.out.println(rd.readLine());

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
        }
    }

    public List<Submission> getGradesForAssignment(Long assignmentID) {
        List<Submission> submissionList;

        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();

            String requestString = "http://localhost:8080/submissions/%7BassignmentID%7D?assignmentID=" + assignmentID;

            HttpGet request = new HttpGet(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String result;
            result = rd.readLine();

            submissionList = objectMapper.readValue(result, new TypeReference<List<Submission>>() {
            });

            System.out.println(submissionList);
            return submissionList;

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void saveSubmission(Long studentID, Long assignmentID, Date date, String description) {
        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();

            String requestString = "http://localhost:8080/submissions?studentID=" + studentID + "&assignmentID=" + assignmentID + "&date=" + date + "&description=" + description;

            HttpPost request = new HttpPost(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            System.out.println(rd.readLine());

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
        }
    }

    public void updateSubmission(Long submissionID, Long studentID, Long assignmentID, Date date, String description) {
        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();

            String requestString = "http://localhost:8080/submissions?submissionID=" + submissionID + "&studentID=" + studentID + "&assignmentID=" + assignmentID + "&date=" + date + "&description=" + description;

            HttpPut request = new HttpPut(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            System.out.println(rd.readLine());

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteSubmissionByID(Long submissionID) {
        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();

            String requestString = "http://localhost:8080/submissions?submissionID=" + submissionID;

            HttpDelete request = new HttpDelete(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            System.out.println(rd.readLine());
        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
        }
    }
}
