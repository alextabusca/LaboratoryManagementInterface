package HttpResponse;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.Professor;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class ProfessorResponse {

    private ObjectMapper objectMapper = new ObjectMapper();

    public ProfessorResponse() {

    }

    public void getAllProfessors() {
        List<Professor> professorList;

        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();

            HttpGet request = new HttpGet("http://localhost:8080/professors/getAllProfessors");
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String result;
            result = rd.readLine();

            professorList = objectMapper.readValue(result, new TypeReference<List<Professor>>() {
            });

            System.out.println(professorList);

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
        }
    }

    public String getProfessorByID(Long professorID) {
        new Professor();
        Professor professor;

        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();

            String requestString = "http://localhost:8080/professors/%7BprofessorID%7D?professorID=" + professorID;
            HttpGet request = new HttpGet(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String result;
            result = rd.readLine();

            professor = objectMapper.readValue(result, Professor.class);
            System.out.println(professor.toString());

            return result;

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String loginProfessor(String email, String password) {

        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();

            StringBuilder requestString = new StringBuilder();
            requestString.append("http://localhost:8080/professors?email=");
            requestString.append(email);
            requestString.append("&password=");
            requestString.append(password);

            System.out.println(requestString);

            HttpGet request = new HttpGet(requestString.toString());

            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String result;
            result = rd.readLine();

            return result;

            /*if (result != null && !result.isEmpty())
                System.out.println(result);
            else
                System.out.println("Invalid credentials!");*/

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void saveProfessor(String firstName, String lastName, String email, String password) {
        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();

            String requestString = "http://localhost:8080/professors?first%20name=" + firstName + "&last%20name=" + lastName + "&email=" + email + "&password=" + password;

            HttpPost request = new HttpPost(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            System.out.println(rd.readLine());

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
        }
    }

    public void updateProfessor(Long professorID, String firstName, String lastName, String email, String password) {
        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();

            String requestString = "http://localhost:8080/professors?professorID=" + professorID + "first%20name=" + firstName + "&last%20name=" + lastName + "&email=" + email + "&password=" + password;

            HttpPut request = new HttpPut(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            System.out.println(rd.readLine());

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
        }
    }


    public void deleteProfessorByID(Long professorID) {
        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();

            String requestString = "http://localhost:8080/professors?professorID=" + professorID;

            HttpDelete request = new HttpDelete(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            System.out.println(rd.readLine());
        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
        }
    }
}
