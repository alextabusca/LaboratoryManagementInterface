package HttpResponse;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.Laboratory;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Date;
import java.util.List;

public class LaboratoryResponse {

    private ObjectMapper objectMapper = new ObjectMapper();

    public LaboratoryResponse() {

    }

    public List<Laboratory> getAllLaboratories() {
        List<Laboratory> laboratoryList;

        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet("http://localhost:8080/laboratories");
            request.addHeader("accept", "*/*");
            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String result;
            result = rd.readLine();

            laboratoryList = objectMapper.readValue(result, new TypeReference<List<Laboratory>>() {
            });

            return laboratoryList;

            //System.out.println(laboratoryList);
            //System.out.println(result);

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Laboratory getLaboratoryByID(Long laboratoryID) {
        new Laboratory();
        Laboratory laboratory;

        try {

            CloseableHttpClient client = HttpClientBuilder.create().build();

            String requestString = "http://localhost:8080/laboratories/%7BlaboratoryID%7D?laboratoryID=" + laboratoryID;
            HttpGet request = new HttpGet(requestString);
            request.addHeader("accept", "*/*");
            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String result;
            result = rd.readLine();

            laboratory = objectMapper.readValue(result, Laboratory.class);
            System.out.println(laboratory.toString());

            return laboratory;

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Laboratory> searchAfterKeyword(String keyword) {
        List<Laboratory> laboratoryList;

        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();

            String requestString = "http://localhost:8080/laboratories/%7Bkeyword%7D?keyword=" + keyword;

            HttpGet request = new HttpGet(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String result;
            result = rd.readLine();

            laboratoryList = objectMapper.readValue(result, new TypeReference<List<Laboratory>>() {
            });

            return laboratoryList;

            //System.out.println(laboratoryList);

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void saveLaboratory(String title, String mainTopics, String longDescription, Date date) {
        try {

            CloseableHttpClient client = HttpClientBuilder.create().build();
            String requestString = "http://localhost:8080/laboratories?title=" + title + "&date=" + date + "&mainTopics=" + mainTopics + "&longDescription=" + longDescription;

            HttpPost request = new HttpPost(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            System.out.println(rd.readLine());

            /*SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
            Date parsedate = null;
            try {
                parsedate = format.parse("2019-01-01");
            } catch (ParseException e) {
                e.printStackTrace();
            }
            java.sql.Date sql = new java.sql.Date(parsedate.getTime());*/

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
        }
    }

    public void updateLaboratory(Long laboratoryID, String title, String mainTopics, String longDescription, Date date) {
        try {

            CloseableHttpClient client = HttpClientBuilder.create().build();
            String requestString = "http://localhost:8080/laboratories?laboratoryID=" + laboratoryID + "&title=" + title + "&date=" + date + "&mainTopics=" + mainTopics + "&longDescription=" + longDescription;

            HttpPut request = new HttpPut(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            System.out.println(rd.readLine());

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteLaboratoryByID(Long laboratoryID) {
        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();

            String requestString = "http://localhost:8080/laboratories?laboratoryID=" + laboratoryID;

            HttpDelete request = new HttpDelete(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            System.out.println(rd.readLine());
        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
        }
    }
}
