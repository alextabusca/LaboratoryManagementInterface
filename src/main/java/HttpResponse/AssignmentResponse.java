package HttpResponse;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.Assignment;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Date;
import java.util.List;

public class AssignmentResponse {

    private ObjectMapper objectMapper = new ObjectMapper();

    public AssignmentResponse() {

    }

    public List<Assignment> getAllAssignments() {
        List<Assignment> assignmentList;

        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet("http://localhost:8080/assignments/getAllAssignments");
            request.addHeader("accept", "*/*");
            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String result;
            result = rd.readLine();

            assignmentList = objectMapper.readValue(result, new TypeReference<List<Assignment>>() {
            });

            System.out.println(assignmentList);
            return assignmentList;

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Assignment getAssignmentByID(Long assignmentID) {
        Assignment assignment;

        try {

            CloseableHttpClient client = HttpClientBuilder.create().build();

            String requestString = "http://localhost:8080/assignments/%7BassignmentID%7D?assignmentID=" + assignmentID;

            HttpGet request = new HttpGet(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String result;
            result = rd.readLine();

            assignment = objectMapper.readValue(result, Assignment.class);
            System.out.println(assignment.toString());

            return assignment;

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Assignment> getAssignmentsByLaboratoryID(Long laboratoryID) {
        List<Assignment> assignmentList;

        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();

            String requestString = "http://localhost:8080/assignments/%7BlaboratoryID%7D?laboratoryID=" + laboratoryID;

            HttpGet request = new HttpGet(requestString);
            request.addHeader("accept", "*/*");
            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String result;
            result = rd.readLine();

            assignmentList = objectMapper.readValue(result, new TypeReference<List<Assignment>>() {
            });

            System.out.println(assignmentList);
            return assignmentList;

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void saveAssignment(String title, Date deadline, String description, Long laboratoryID) {
        try {

            CloseableHttpClient client = HttpClientBuilder.create().build();
            String requestString = "http://localhost:8080/assignments?title=" + title + "&deadline=" + deadline + "&description=" + description + "&laboratoryID=" + laboratoryID;

            HttpPost request = new HttpPost(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            System.out.println(rd.readLine());

            /*SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
            Date parsedate = null;
            try {
                parsedate = format.parse("2019-01-01");
            } catch (ParseException e) {
                e.printStackTrace();
            }
            java.sql.Date sql = new java.sql.Date(parsedate.getTime());*/

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
        }
    }

    public void updateAssignment(Long assignmentID, String title, Date deadline, String description, Long laboratoryID) {
        try {

            CloseableHttpClient client = HttpClientBuilder.create().build();
            String requestString = "http://localhost:8080/assignments?assignmentID=" + assignmentID + "&title=" + title + "&deadline=" + deadline + "&description=" + description + "&laboratoryID=" + laboratoryID;

            HttpPut request = new HttpPut(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            System.out.println(rd.readLine());

            /*SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
            Date parsedate = null;
            try {
                parsedate = format.parse("2019-01-01");
            } catch (ParseException e) {
                e.printStackTrace();
            }
            java.sql.Date sql = new java.sql.Date(parsedate.getTime());*/

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteAssignmentByID(Long assignmentID) {
        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();

            String requestString = "http://localhost:8080/assignments?assignmentID=" + assignmentID;

            HttpDelete request = new HttpDelete(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            System.out.println(rd.readLine());
        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
        }
    }
}
