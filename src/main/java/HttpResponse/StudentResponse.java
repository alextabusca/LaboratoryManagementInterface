package HttpResponse;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.Student;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class StudentResponse {

    private ObjectMapper objectMapper = new ObjectMapper();
    private Long loginStudentID;

    public StudentResponse() {

    }

    public List<Student> getAllStudents() {
        List<Student> studentList;

        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet("http://localhost:8080/students/getAllStudents");
            request.addHeader("accept", "*/*");
            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String result;
            result = rd.readLine();

            studentList = objectMapper.readValue(result, new TypeReference<List<Student>>() {
            });

            System.out.println(studentList);
            return studentList;

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Student getStudentByID(Long studentID) {
        new Student();
        Student student;

        try {

            CloseableHttpClient client = HttpClientBuilder.create().build();

            String requestString = "http://localhost:8080/students/%7BstudentID%7D?studentID=" + studentID;
            HttpGet request = new HttpGet(requestString);
            request.addHeader("accept", "*/*");
            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String result;
            result = rd.readLine();

            student = objectMapper.readValue(result, Student.class);
            System.out.println(student.toString());

            return student;

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void registerStudent(String token, String email, String password, String firstName, String lastName, String inGroup, String hobby) {
        try {

            CloseableHttpClient client = HttpClientBuilder.create().build();

            String requestString = "http://localhost:8080/students/%7Btoken%7D?token=" + token + "&email=" + email + "&firstName=" + firstName + "&lastName=" + lastName + "&inGroup=" + inGroup + "&password=" + password + "&hobby=" + hobby;

            HttpPut request = new HttpPut(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            System.out.println(rd.readLine());

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
        }
    }

    public String loginStudent(String email, String password) {
        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();

            String requestString = "http://localhost:8080/students/loginStudent?email=" + email + "&password=" + password;

            System.out.println(requestString);

            HttpGet request = new HttpGet(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String result;
            result = rd.readLine();

            Student student;
            if (result != null && !result.equals("[]")) {
                student = objectMapper.readValue(result, Student.class);
                loginStudentID = student.getId();
                System.out.println(student.toString());
            }

            return result;

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Long getStudentID() {
        return loginStudentID;
    }

    public void saveStudent(String email) {
        try {

            CloseableHttpClient client = HttpClientBuilder.create().build();
            String requestString = "http://localhost:8080/students/?email=" + email;

            HttpPost request = new HttpPost(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            System.out.println(rd.readLine());

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
        }
    }

    public void updateStudent(Long studentID, String firstName, String lastName, String inGroup, String email, String password, String hobby) {
        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();

            String requestString = "http://localhost:8080/students?studentID=" + studentID + "&firstName=" + firstName + "&lastName=" + lastName + "&inGroup=" + inGroup + "&email=" + email + "&password=" + password + "&hobby=" + hobby;

            HttpPut request = new HttpPut(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            System.out.println(rd.readLine());

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteStudentByID(Long studentID) {
        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();

            String requestString = "http://localhost:8080/students?studentID=" + studentID;

            HttpDelete request = new HttpDelete(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            System.out.println(rd.readLine());
        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
        }
    }
}
