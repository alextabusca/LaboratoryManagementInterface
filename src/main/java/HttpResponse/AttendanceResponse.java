package HttpResponse;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.Attendance;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class AttendanceResponse {

    private ObjectMapper objectMapper = new ObjectMapper();

    public AttendanceResponse() {

    }

    public List<Attendance> getAllAttendances() {
        List<Attendance> attendanceList;

        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet("http://localhost:8080/attendances/getAllAttendances");
            request.addHeader("accept", "*/*");
            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String result;
            result = rd.readLine();

            attendanceList = objectMapper.readValue(result, new TypeReference<List<Attendance>>() {
            });

            System.out.println(attendanceList);
            return attendanceList;

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Attendance getAttendanceByID(Long attendanceID) {
        new Attendance();
        Attendance attendance;

        try {

            CloseableHttpClient client = HttpClientBuilder.create().build();

            String requestString = "http://localhost:8080/attendances/%7BattendanceID%7D?attendanceID=" + attendanceID;
            HttpGet request = new HttpGet(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String result;
            result = rd.readLine();

            attendance = objectMapper.readValue(result, Attendance.class);
            System.out.println(attendance.toString());

            return attendance;

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Attendance> getAttendanceByLaboratoryID(Long laboratoryID) {
        List<Attendance> attendanceList;

        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();

            String requestString = "http://localhost:8080/attendances/%7BlaboratoryID%7D?laboratoryID=" + laboratoryID;

            HttpGet request = new HttpGet(requestString);
            request.addHeader("accept", "*/*");
            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String result;
            result = rd.readLine();

            attendanceList = objectMapper.readValue(result, new TypeReference<List<Attendance>>() {
            });

            System.out.println(attendanceList);

            return attendanceList;

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Attendance> getAttendanceByStudentID(Long studentID) {
        List<Attendance> attendanceList;

        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();

            String requestString = "http://localhost:8080/attendances/%7BstudentID%7D?studentID=" + studentID;

            HttpGet request = new HttpGet(requestString);
            request.addHeader("accept", "*/*");
            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String result;
            result = rd.readLine();

            attendanceList = objectMapper.readValue(result, new TypeReference<List<Attendance>>() {
            });

            System.out.println(attendanceList);

            return attendanceList;

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void saveAttendance(Long studentID, Long laboratoryID, boolean isPresent) {
        try {

            CloseableHttpClient client = HttpClientBuilder.create().build();

            String requestString = "http://localhost:8080/attendances?studentID=" + studentID + "&laboratoryID=" + laboratoryID + "&isPresent=" + isPresent;

            HttpPost request = new HttpPost(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            System.out.println(rd.readLine());

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
        }
    }

    public void updateAttendance(Long attendanceID, Long studentID, Long laboratoryID, boolean isPresent) {
        try {

            CloseableHttpClient client = HttpClientBuilder.create().build();
            String requestString = "http://localhost:8080/attendances?attendanceID=" + attendanceID + "&studentID=" + studentID + "&laboratoryID=" + laboratoryID + "&isPresent=" + isPresent;

            HttpPut request = new HttpPut(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            System.out.println(rd.readLine());

        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteAttendanceByID(Long attendanceID) {
        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();

            String requestString = "http://localhost:8080/attendances?attendanceID=" + attendanceID;

            HttpDelete request = new HttpDelete(requestString);
            request.addHeader("accept", "*/*");

            HttpResponse response = client.execute(request);

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            System.out.println(rd.readLine());
        } catch (UnsupportedOperationException | IOException e) {
            e.printStackTrace();
        }
    }
}
