package Interface;

import HttpResponse.ProfessorResponse;
import HttpResponse.StudentResponse;
import Interface.util.Encryption;

import javax.swing.*;
import java.awt.*;

class LoginUI extends JFrame {

    private static Long studentID;

    LoginUI() {

        super("Laboratory Management - Login");
        setName("Login Interface");

        setSize(400, 125);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setResizable(false);

        JPanel userInfoPanel = new JPanel();
        userInfoPanel.setLayout(new GridLayout(2, 2));

        JLabel emailLabel = new JLabel("Email: ");
        JLabel passwordLabel = new JLabel("Password: ");

        JTextField emailText = new JTextField(30);
        JPasswordField passwordText = new JPasswordField(30);

        userInfoPanel.add(emailLabel);
        userInfoPanel.add(emailText);
        userInfoPanel.add(passwordLabel);
        userInfoPanel.add(passwordText);

        JPanel loginButtonPanel = new JPanel();
        JButton loginButton = new JButton("Login");
        loginButtonPanel.add(loginButton);

        loginButton.addActionListener(e -> {
            StudentResponse studentResponse = new StudentResponse();
            ProfessorResponse professorResponse = new ProfessorResponse();

            @SuppressWarnings("deprecation") String resultStudent = studentResponse.loginStudent(emailText.getText(), Encryption.passwordEncryption(passwordText.getText()));
            @SuppressWarnings("deprecation") String resultProfessor = professorResponse.loginProfessor(emailText.getText(), Encryption.passwordEncryption(passwordText.getText()));

            emailText.setText("");
            passwordText.setText("");

            if (resultStudent != null && !resultStudent.isEmpty() && !resultStudent.equals("[]")) {
                System.out.println(resultStudent);
                studentID = studentResponse.getStudentID();
                new StudentUI();
            } else if (resultProfessor != null && !resultProfessor.isEmpty() && !resultProfessor.equals("[]")) {
                System.out.println(resultProfessor);
                new ProfessorUI();
            } else
                JOptionPane.showMessageDialog(null, "Invalid credentials!");
            this.dispose();
        });

        add(userInfoPanel, BorderLayout.CENTER);
        add(loginButton, BorderLayout.SOUTH);

        setLocationRelativeTo(null);
        setVisible(true);
    }

    static Long getStudentID() {
        return studentID;
    }
}
