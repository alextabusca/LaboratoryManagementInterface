package Interface;

import javax.swing.*;
import java.awt.*;

public class WelcomeUI extends JFrame {

    public WelcomeUI() {

        super("Laboratory Management - Welcome");
        setName("Welcome");

        setSize(400, 125);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);

        JButton loginButton = new JButton("Login");
        JButton registerButton = new JButton("Register");

        loginButton.addActionListener(e -> new LoginUI());
        registerButton.addActionListener(e -> new RegisterUI());

        add(loginButton, BorderLayout.EAST);
        add(registerButton, BorderLayout.WEST);

        setLocationRelativeTo(null);
        setVisible(true);
    }
}
