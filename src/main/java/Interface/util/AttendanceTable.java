package Interface.util;

import HttpResponse.AttendanceResponse;
import model.Attendance;

import javax.swing.*;
import java.util.List;

public class AttendanceTable extends AbstractTable<Attendance> {

    private AttendanceResponse attendanceResponse;
    private List<Attendance> attendanceList;

    private JTable create(List<Attendance> attendanceList) {
        AttendanceTable attendanceTable = new AttendanceTable();

        return attendanceTable.createTable(attendanceList);
    }

    public JTable createTableByStudent(Long studentID) {
        attendanceResponse = new AttendanceResponse();
        attendanceList = attendanceResponse.getAttendanceByStudentID(studentID);

        return create(attendanceList);
    }

    public JTable createTableByLaboratory(Long laboratoryID) {
        attendanceResponse = new AttendanceResponse();
        attendanceList = attendanceResponse.getAttendanceByLaboratoryID(laboratoryID);

        return create(attendanceList);
    }

    public JTable createTable() {
        attendanceResponse = new AttendanceResponse();
        attendanceList = attendanceResponse.getAllAttendances();

        return create(attendanceList);
    }

}
