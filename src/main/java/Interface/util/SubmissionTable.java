package Interface.util;

import HttpResponse.SubmissionResponse;
import model.Submission;

import javax.swing.*;
import java.util.List;

public class SubmissionTable extends AbstractTable<Submission> {

    public JTable create(List<Submission> submissionList) {
        SubmissionTable submissionTable = new SubmissionTable();

        return submissionTable.createTable(submissionList);
    }

    public JTable createSubmissionsByAssignment(Long assignmentID) {
        SubmissionResponse submissionResponse = new SubmissionResponse();
        List<Submission> submissionList = submissionResponse.getGradesForAssignment(assignmentID);

        return create(submissionList);
    }
}
