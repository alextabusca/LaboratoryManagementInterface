package Interface.util;

import HttpResponse.AssignmentResponse;
import model.Assignment;

import javax.swing.*;
import java.util.List;

public class AssignmentTable extends AbstractTable<Assignment> {

    public JTable create(List<Assignment> assignmentList) {
        AssignmentTable assignmentTable = new AssignmentTable();

        return assignmentTable.createTable(assignmentList);
    }

    public JTable createAllAssignments() {
        AssignmentResponse assignmentResponse = new AssignmentResponse();
        List<Assignment> assignmentList = assignmentResponse.getAllAssignments();

        return create(assignmentList);
    }

}
