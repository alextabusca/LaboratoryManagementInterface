package Interface.util;

import HttpResponse.LaboratoryResponse;
import model.Laboratory;

import javax.swing.*;
import java.util.List;

public class LaboratoryTable extends AbstractTable<Laboratory> {

    private LaboratoryResponse laboratoryResponse;
    private List<Laboratory> laboratoryList;

    private JTable create(List<Laboratory> laboratoryList) {
        LaboratoryTable laboratoryTable = new LaboratoryTable();

        return laboratoryTable.createTable(laboratoryList);
    }

    public JTable createTableAll() {
        laboratoryResponse = new LaboratoryResponse();
        laboratoryList = laboratoryResponse.getAllLaboratories();

        return create(laboratoryList);
    }

    public JTable createTableSearch(String keyword) {
        laboratoryResponse = new LaboratoryResponse();
        laboratoryList = laboratoryResponse.searchAfterKeyword(keyword);

        return create(laboratoryList);
    }
}
