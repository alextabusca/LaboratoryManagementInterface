package Interface.util;

import HttpResponse.StudentResponse;
import model.Student;

import javax.swing.*;
import java.util.List;

public class StudentTable extends AbstractTable<Student> {

    private JTable create(List<Student> studentList) {
        StudentTable studentTable = new StudentTable();

        return studentTable.createTable(studentList);
    }

    public JTable createTable() {
        StudentResponse studentResponse = new StudentResponse();
        List<Student> studentList = studentResponse.getAllStudents();

        return create(studentList);
    }
}
