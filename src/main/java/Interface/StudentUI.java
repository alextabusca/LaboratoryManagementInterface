package Interface;

import HttpResponse.AssignmentResponse;
import HttpResponse.LaboratoryResponse;
import HttpResponse.StudentResponse;
import HttpResponse.SubmissionResponse;
import Interface.util.AssignmentTable;
import Interface.util.LaboratoryTable;
import model.Assignment;
import model.Laboratory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.util.List;

class StudentUI extends JFrame {

    private LaboratoryTable laboratories;
    private AssignmentTable assignments;
    private Long studentID;

    private JPanel laboratoryTablePanel, assignmentTablePanel;

    private JTable newLaboratoryTable, newAssignmentTable;

    StudentUI() {

        super("Laboratory Management - Student");
        setName("Student Interface");

        setSize(1000, 800);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setResizable(false);

        JPanel finalPanel = new JPanel();

        StudentResponse studentResponse = new StudentResponse();
        studentID = studentResponse.getStudentID();

        /*
        Laboratory Panel
         */
        JPanel laboratoryPanel = new JPanel();
        laboratoryTablePanel = new JPanel();
        JButton viewAllLaboratories = new JButton("View all laboratories");

        laboratories = new LaboratoryTable();
        JTable laboratoryTable = laboratories.createTableAll();
        laboratoryTable.setPreferredSize(new Dimension(600, 300));
        laboratoryTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        JScrollPane laboratoryScrollPane = new JScrollPane(laboratoryTable);
        laboratoryScrollPane.setPreferredSize(new Dimension(600, 300));

        viewAllLaboratories.addActionListener(e -> refreshLaboratoryTable());

        JTextField searchText = new JTextField("Insert keyword here", 10);
        searchText.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                searchText.setText("");
            }
        });

        JButton searchButton = new JButton("Search");
        searchButton.addActionListener(e -> refreshLaboratoryTableWithSearch(searchText.getText()));

        laboratoryTablePanel.add(laboratoryScrollPane);
        laboratoryPanel.add(laboratoryTablePanel);
        laboratoryPanel.add(viewAllLaboratories);
        laboratoryPanel.add(searchText);
        laboratoryPanel.add(searchButton);


        /*
        Assignment Panel
         */
        JPanel assignmentPanel = new JPanel();
        assignmentTablePanel = new JPanel();
        JButton viewAssignments = new JButton("View assignments");

        assignments = new AssignmentTable();
        JTable assignmentTable = new JTable();
        assignmentTable.setPreferredSize(new Dimension(600, 300));
        assignmentTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        JScrollPane assignmentScrollPane = new JScrollPane(assignmentTable);
        assignmentScrollPane.setPreferredSize(new Dimension(600, 300));

        viewAssignments.addActionListener(e -> {
            LaboratoryResponse laboratoryResponse = new LaboratoryResponse();
            AssignmentResponse assignmentResponse = new AssignmentResponse();
            Laboratory laboratory;
            List<Assignment> assignmentList;

            laboratory = laboratoryResponse.getLaboratoryByID((Long) newLaboratoryTable.getValueAt(newLaboratoryTable.getSelectedRow(), 0));
            assignmentList = assignmentResponse.getAssignmentsByLaboratoryID(laboratory.getId());
            refreshAssignmentTable(assignmentList);
        });

        assignmentTablePanel.add(assignmentScrollPane);
        assignmentPanel.add(assignmentTablePanel);
        assignmentPanel.add(viewAssignments);


        /*
        Submission Panel
         */
        JPanel submissionPanel = new JPanel();
        JTextField submissionText = new JTextField("Git link", 10);
        submissionText.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                submissionText.setText("");
            }
        });
        JButton submitButton = new JButton("Submit");
        submitButton.addActionListener(e -> {
            SubmissionResponse submissionResponse = new SubmissionResponse();
            AssignmentResponse assignmentResponse = new AssignmentResponse();
            Assignment assignment;

            assignment = assignmentResponse.getAssignmentByID((Long) newAssignmentTable.getValueAt(newAssignmentTable.getSelectedRow(), 0));
            System.out.println(assignment.toString());

            long millis = System.currentTimeMillis();
            java.sql.Date date = new java.sql.Date(millis);

            submissionResponse.saveSubmission(LoginUI.getStudentID(), assignment.getId(), date, submissionText.getText());
            System.out.println(studentResponse.getStudentID());
        });
        submissionPanel.add(submissionText);
        submissionPanel.add(submitButton);

        /*
        Final stuff
         */
        finalPanel.add(laboratoryPanel);
        finalPanel.add(assignmentPanel);
        finalPanel.add(submissionPanel, BorderLayout.SOUTH);
        add(finalPanel);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    @SuppressWarnings("Duplicates")
    private void refreshLaboratoryTable() {
        newLaboratoryTable = laboratories.createTableAll();
        newLaboratoryTable.setPreferredSize(new Dimension(600, 500));
        newLaboratoryTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));

        JScrollPane newPane = new JScrollPane(newLaboratoryTable);
        newPane.setPreferredSize(new Dimension(600, 300));

        laboratoryTablePanel.removeAll();
        laboratoryTablePanel.add(newPane);
        laboratoryTablePanel.revalidate();
        laboratoryTablePanel.repaint();
    }

    private void refreshLaboratoryTableWithSearch(String keyword) {
        newLaboratoryTable = laboratories.createTableSearch(keyword);
        newLaboratoryTable.setPreferredSize(new Dimension(600, 500));
        newLaboratoryTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));

        JScrollPane newPane = new JScrollPane(newLaboratoryTable);
        newPane.setPreferredSize(new Dimension(600, 300));

        laboratoryTablePanel.removeAll();
        laboratoryTablePanel.add(newPane);
        laboratoryTablePanel.revalidate();
        laboratoryTablePanel.repaint();
    }

    @SuppressWarnings("Duplicates")
    private void refreshAssignmentTable(List<Assignment> assignmentList) {
        newAssignmentTable = assignments.create(assignmentList);
        newAssignmentTable.setPreferredSize(new Dimension(600, 500));
        newAssignmentTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        JScrollPane newPane = new JScrollPane(newAssignmentTable);
        newPane.setPreferredSize(new Dimension(600, 300));

        assignmentTablePanel.removeAll();
        assignmentTablePanel.add(newPane);
        assignmentTablePanel.revalidate();
        assignmentTablePanel.repaint();
    }

}
