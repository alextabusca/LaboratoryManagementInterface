package Interface;

import HttpResponse.*;
import Interface.util.*;
import model.Assignment;
import model.Attendance;
import model.Laboratory;
import model.Student;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

class ProfessorUI extends JFrame {

    private StudentTable students;
    private LaboratoryTable laboratories;
    private AttendanceTable attendances;
    private AssignmentTable assignments;

    private JTable newLaboratoryTable, newAssignmentTable, newStudentTable, newAttendanceTable, newSubmissionTable, newAssignmentTableInSubmission;

    private JPanel finalPanel, studentPanel, studentTablePanel, laboratoryPanel, laboratoryTablePanel, attendancePanel, attendanceTablePanel, assignmentPanel, assignmentTablePanel, submissionPanel, submissionTablePanel, assignmentTablePanelInSubmission;
    private JButton switchToStudentPanel, switchToLaboratoryPanel, switchToAttendancePanel, switchToAssignmentPanel, switchToSubmissionPanel;

    ProfessorUI() {

        super("Laboratory Management - Teacher");
        setName("Teacher Interface");
        setSize(1200, 450);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setResizable(false);
        setLayout(new GridLayout(1, 4));

        finalPanel = new JPanel();
        switchToStudentPanel = new JButton("Student");
        switchToLaboratoryPanel = new JButton("Laboratory");
        switchToAttendancePanel = new JButton("Attendance");
        switchToAssignmentPanel = new JButton("Assignment");
        switchToSubmissionPanel = new JButton("Submission");

        finalPanel.add(switchToStudentPanel);
        finalPanel.add(switchToLaboratoryPanel);
        finalPanel.add(switchToAttendancePanel);
        finalPanel.add(switchToAssignmentPanel);
        finalPanel.add(switchToSubmissionPanel);

        /*
        Panel switches
         */
        switchToStudentPanel.addActionListener(e -> {
            finalPanel.removeAll();
            finalPanel.add(switchToStudentPanel);
            finalPanel.add(switchToLaboratoryPanel);
            finalPanel.add(switchToAttendancePanel);
            finalPanel.add(switchToAssignmentPanel);
            finalPanel.add(switchToSubmissionPanel);
            finalPanel.add(studentPanel);
            finalPanel.revalidate();
            finalPanel.repaint();
            refreshStudentTable();
        });

        switchToLaboratoryPanel.addActionListener(e -> {
            finalPanel.removeAll();
            finalPanel.add(switchToStudentPanel);
            finalPanel.add(switchToLaboratoryPanel);
            finalPanel.add(switchToAttendancePanel);
            finalPanel.add(switchToAssignmentPanel);
            finalPanel.add(switchToSubmissionPanel);
            finalPanel.add(laboratoryPanel);
            finalPanel.revalidate();
            finalPanel.repaint();
            refreshLaboratoryTable();
        });

        switchToAttendancePanel.addActionListener(e -> {
            finalPanel.removeAll();
            finalPanel.add(switchToStudentPanel);
            finalPanel.add(switchToLaboratoryPanel);
            finalPanel.add(switchToAttendancePanel);
            finalPanel.add(switchToAssignmentPanel);
            finalPanel.add(switchToSubmissionPanel);
            finalPanel.add(attendancePanel);
            finalPanel.revalidate();
            finalPanel.repaint();
            refreshAttendanceTable();
        });

        switchToAssignmentPanel.addActionListener(e -> {
            finalPanel.removeAll();
            finalPanel.add(switchToStudentPanel);
            finalPanel.add(switchToLaboratoryPanel);
            finalPanel.add(switchToAttendancePanel);
            finalPanel.add(switchToAssignmentPanel);
            finalPanel.add(switchToSubmissionPanel);
            finalPanel.add(assignmentPanel);
            finalPanel.revalidate();
            finalPanel.repaint();
            refreshAssignmentTable();
        });

        switchToSubmissionPanel.addActionListener(e -> {
            finalPanel.removeAll();
            finalPanel.add(switchToStudentPanel);
            finalPanel.add(switchToLaboratoryPanel);
            finalPanel.add(switchToAttendancePanel);
            finalPanel.add(switchToAssignmentPanel);
            finalPanel.add(switchToSubmissionPanel);
            finalPanel.add(submissionPanel);
            finalPanel.revalidate();
            finalPanel.repaint();

            refreshAssignmentTableInSubmission();
            clearSubmissions();
        });




        /*
        Student panel
         */
        StudentResponse studentResponse = new StudentResponse();
        studentTablePanel = new JPanel();
        studentPanel = new JPanel();

        JButton addStudentButton = new JButton("Add student");
        JButton editStudentButton = new JButton("Edit student");
        JButton deleteStudentButton = new JButton("Delete student");

        students = new StudentTable();

        JTable studentTable = students.createTable();
        studentTable.setPreferredSize(new Dimension(600, 300));
        studentTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));

        JScrollPane studentScrollPane = new JScrollPane(studentTable);
        studentScrollPane.setPreferredSize(new Dimension(600, 300));

        studentTablePanel.add(studentScrollPane);
        studentPanel.add(studentTablePanel);
        studentPanel.add(addStudentButton);
        studentPanel.add(editStudentButton);
        studentPanel.add(deleteStudentButton);

        addStudentButton.addActionListener(e -> {
            JFrame addStudentFrame = new JFrame();
            JPanel addStudentInfo = new JPanel();
            JPanel addStudentPanel = new JPanel(new GridLayout(2, 0));
            JPanel addStudentSubmitBtn = new JPanel();

            addStudentFrame.setName("Add student");
            addStudentFrame.setSize(400, 150);
            addStudentFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            addStudentFrame.setResizable(false);

            JLabel studentEmail = new JLabel("Email:");
            JTextField studentEmailText = new JTextField("", 10);
            JButton addStudentSubmit = new JButton("Submit");

            addStudentInfo.add(studentEmail);
            addStudentInfo.add(studentEmailText);
            addStudentSubmitBtn.add(addStudentSubmit);
            addStudentPanel.add(addStudentInfo);
            addStudentPanel.add(addStudentSubmitBtn);

            addStudentSubmit.addActionListener(e18 -> {
                studentResponse.saveStudent(studentEmailText.getText());
                refreshStudentTable();
                addStudentFrame.dispose();
            });

            addStudentFrame.add(addStudentPanel);
            addStudentFrame.setVisible(true);
            addStudentFrame.setLocationRelativeTo(null);

        });

        editStudentButton.addActionListener(e -> {
            JFrame editStudentFrame = new JFrame();
            JPanel editStudentPanel = new JPanel();
            editStudentFrame.setName("Edit student");
            editStudentFrame.setSize(450, 200);
            editStudentFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            editStudentFrame.setResizable(false);

            JLabel firstNameLabel = new JLabel("First Name: ");
            JLabel lastNameLabel = new JLabel("Last Name: ");
            JLabel emailLabel = new JLabel("Email: ");
            JLabel passwordLabel = new JLabel("Password: ");
            JLabel groupLabel = new JLabel("Group: ");
            JLabel hobbyLabel = new JLabel("Hobby: ");

            JTextField firstNameText = new JTextField(15);
            JTextField lastNameText = new JTextField(15);
            JTextField emailText = new JTextField(15);
            JPasswordField passwordText = new JPasswordField(15);
            JTextField groupText = new JTextField(15);
            JTextField hobbyText = new JTextField(15);

            Student selectedStudent = studentResponse.getStudentByID((Long) newStudentTable.getValueAt(newStudentTable.getSelectedRow(), 0));

            firstNameText.setText(selectedStudent.getFirstName());
            lastNameText.setText(selectedStudent.getLastName());
            emailText.setText(selectedStudent.getEmail());
            groupText.setText(selectedStudent.getInGroup());
            hobbyText.setText(selectedStudent.getHobby());

            JPanel studentInfoPanel = new JPanel(new GridLayout(6, 2));
            studentInfoPanel.add(firstNameLabel);
            studentInfoPanel.add(firstNameText);
            studentInfoPanel.add(lastNameLabel);
            studentInfoPanel.add(lastNameText);
            studentInfoPanel.add(emailLabel);
            studentInfoPanel.add(emailText);
            studentInfoPanel.add(passwordLabel);
            studentInfoPanel.add(passwordText);
            studentInfoPanel.add(groupLabel);
            studentInfoPanel.add(groupText);
            studentInfoPanel.add(hobbyLabel);
            studentInfoPanel.add(hobbyText);

            JPanel submitEditPanel = new JPanel();
            JButton submitButton = new JButton("Submit");
            submitButton.addActionListener(e1 -> {
                //noinspection deprecation
                studentResponse.updateStudent(selectedStudent.getId(), firstNameText.getText(), lastNameText.getText(), groupText.getText(), emailText.getText(), passwordText.getText(), hobbyText.getText());
                refreshStudentTable();
                editStudentFrame.dispose();
            });

            submitEditPanel.add(submitButton);
            editStudentPanel.add(studentInfoPanel);
            editStudentPanel.add(submitEditPanel);
            editStudentFrame.add(editStudentPanel);
            editStudentFrame.setVisible(true);
            editStudentFrame.setLocationRelativeTo(null);
        });

        deleteStudentButton.addActionListener(e -> {
            studentResponse.deleteStudentByID((Long) newStudentTable.getValueAt(newStudentTable.getSelectedRow(), 0));
            refreshStudentTable();
        });




        /*
        Laboratory panel
         */
        LaboratoryResponse laboratoryResponse = new LaboratoryResponse();
        laboratoryPanel = new JPanel();
        laboratoryTablePanel = new JPanel();

        JButton addLaboratoryButton = new JButton("Add laboratory");
        JButton editLaboratoryButton = new JButton("Edit laboratory");
        JButton deleteLaboratoryButton = new JButton("Delete laboratory");

        laboratories = new LaboratoryTable();
        JTable laboratoryTable = laboratories.createTableAll();
        laboratoryTable.setPreferredSize(new Dimension(600, 300));
        laboratoryTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));

        JScrollPane laboratoryScrollPane = new JScrollPane(laboratoryTable);
        laboratoryScrollPane.setPreferredSize(new Dimension(600, 300));

        laboratoryTablePanel.add(laboratoryScrollPane);
        laboratoryPanel.add(laboratoryTablePanel);
        laboratoryPanel.add(addLaboratoryButton);
        laboratoryPanel.add(editLaboratoryButton);
        laboratoryPanel.add(deleteLaboratoryButton);

        addLaboratoryButton.addActionListener(e -> {
            JFrame addLaboratoryFrame = new JFrame();
            JPanel addLaboratoryPanel = new JPanel();
            addLaboratoryFrame.setName("Add laboratory");
            addLaboratoryFrame.setSize(450, 130);
            addLaboratoryFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            addLaboratoryFrame.setResizable(false);

            JPanel laboratoryInfo = new JPanel(new GridLayout(4, 2));
            JLabel titleLabel = new JLabel("Title: ");
            JLabel dateLabel = new JLabel("Date: ");
            JLabel mainTopicsLabel = new JLabel("Main topics: ");
            JLabel descriptionLabel = new JLabel("Description: ");
            JTextField titleText = new JTextField(15);
            JTextField dateText = new JTextField(15);
            JTextField mainTopicsText = new JTextField(15);
            JTextField descriptionText = new JTextField(15);
            laboratoryInfo.add(titleLabel);
            laboratoryInfo.add(titleText);
            laboratoryInfo.add(dateLabel);
            laboratoryInfo.add(dateText);
            laboratoryInfo.add(mainTopicsLabel);
            laboratoryInfo.add(mainTopicsText);
            laboratoryInfo.add(descriptionLabel);
            laboratoryInfo.add(descriptionText);

            JPanel submitButtonPanel = new JPanel();
            JButton submitButton = new JButton("Submit");
            submitButtonPanel.add(submitButton);
            submitButton.addActionListener(e12 -> {
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
                Date parsedate = null;
                try {
                    parsedate = format.parse(dateText.getText());
                } catch (ParseException e2) {
                    e2.printStackTrace();
                }
                java.sql.Date sqlDate = null;
                if (parsedate != null) {
                    sqlDate = new java.sql.Date(parsedate.getTime());
                }
                laboratoryResponse.saveLaboratory(titleText.getText(), mainTopicsText.getText(), descriptionText.getText(), sqlDate);
                refreshLaboratoryTable();
                addLaboratoryFrame.dispose();
            });

            addLaboratoryPanel.add(laboratoryInfo);
            addLaboratoryPanel.add(submitButtonPanel);
            addLaboratoryFrame.add(addLaboratoryPanel);
            addLaboratoryFrame.setVisible(true);
            addLaboratoryFrame.setLocationRelativeTo(null);
        });

        editLaboratoryButton.addActionListener(e -> {
            JFrame editLaboratoryFrame = new JFrame();
            JPanel editLaboratoryPanel = new JPanel();
            editLaboratoryFrame.setName("Add laboratory");
            editLaboratoryFrame.setSize(450, 130);
            editLaboratoryFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            editLaboratoryFrame.setResizable(false);

            JPanel laboratoryInfo = new JPanel(new GridLayout(4, 2));
            JLabel titleLabel = new JLabel("Title: ");
            JLabel dateLabel = new JLabel("Date: ");
            JLabel mainTopicsLabel = new JLabel("Main topics: ");
            JLabel descriptionLabel = new JLabel("Description: ");
            JTextField titleText = new JTextField(15);
            JTextField dateText = new JTextField(15);
            JTextField mainTopicsText = new JTextField(15);
            JTextField descriptionText = new JTextField(15);

            Laboratory selectedLaboratory = laboratoryResponse.getLaboratoryByID((Long) newLaboratoryTable.getValueAt(newLaboratoryTable.getSelectedRow(), 0));
            titleText.setText(selectedLaboratory.getTitle());
            dateText.setText(selectedLaboratory.getDate().toString());
            mainTopicsText.setText(selectedLaboratory.getMainTopics());
            descriptionText.setText(selectedLaboratory.getLongDescription());
            laboratoryInfo.add(titleLabel);
            laboratoryInfo.add(titleText);
            laboratoryInfo.add(dateLabel);
            laboratoryInfo.add(dateText);
            laboratoryInfo.add(mainTopicsLabel);
            laboratoryInfo.add(mainTopicsText);
            laboratoryInfo.add(descriptionLabel);
            laboratoryInfo.add(descriptionText);

            JPanel submitButtonPanel = new JPanel();
            JButton submitButton = new JButton("Submit");
            submitButtonPanel.add(submitButton);
            submitButton.addActionListener(e13 -> {
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
                Date parsedate = null;
                try {
                    parsedate = format.parse(dateText.getText());
                } catch (ParseException e2) {
                    e2.printStackTrace();
                }
                java.sql.Date sqlDate = null;
                if (parsedate != null) {
                    sqlDate = new java.sql.Date(parsedate.getTime());
                }
                laboratoryResponse.updateLaboratory(selectedLaboratory.getId(), titleText.getText(), mainTopicsText.getText(), descriptionText.getText(), sqlDate);
                refreshLaboratoryTable();
                editLaboratoryFrame.dispose();
            });

            editLaboratoryPanel.add(laboratoryInfo);
            editLaboratoryPanel.add(submitButtonPanel);
            editLaboratoryFrame.add(editLaboratoryPanel);
            editLaboratoryFrame.setVisible(true);
            editLaboratoryFrame.setLocationRelativeTo(null);
        });

        deleteLaboratoryButton.addActionListener(e -> {
            laboratoryResponse.deleteLaboratoryByID((Long) newLaboratoryTable.getValueAt(newLaboratoryTable.getSelectedRow(), 0));
            refreshLaboratoryTable();
        });



        /*
        Attendance panel
         */
        AttendanceResponse attendanceResponse = new AttendanceResponse();
        attendancePanel = new JPanel(new GridLayout(1, 0));
        JPanel attendancePanelButtons = new JPanel(new GridLayout(3, 0));
        JPanel actionButtons = new JPanel();
        JPanel byLaboratoryButtons = new JPanel();
        JPanel byStudentButtons = new JPanel();
        attendanceTablePanel = new JPanel();

        JButton addAttendanceButton = new JButton("Add attendance");
        JButton editAttendanceButton = new JButton("Edit attendance");
        JButton deleteAttendanceButton = new JButton("Delete attendance");
        JButton getByLaboratory = new JButton("Get by laboratory");
        JButton getByStudent = new JButton("Get by student");
        JTextField laboratoryNumber = new JTextField("Enter laboratory number", 15);
        JTextField studentNumber = new JTextField("Enter student number", 15);

        attendances = new AttendanceTable();
        JTable attendanceTable = new JTable();
        attendanceTable.setPreferredSize(new Dimension(500, 300));
        attendanceTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));

        JScrollPane attendanceScrollPane = new JScrollPane(attendanceTable);
        attendanceScrollPane.setPreferredSize(new Dimension(500, 300));

        attendanceTablePanel.add(attendanceScrollPane);
        attendancePanel.add(attendanceTablePanel);

        actionButtons.add(addAttendanceButton);
        actionButtons.add(editAttendanceButton);
        actionButtons.add(deleteAttendanceButton);

        byLaboratoryButtons.add(getByLaboratory);
        byLaboratoryButtons.add(laboratoryNumber);

        byStudentButtons.add(getByStudent);
        byStudentButtons.add(studentNumber);

        attendancePanelButtons.add(actionButtons);
        attendancePanelButtons.add(byLaboratoryButtons);
        attendancePanelButtons.add(byStudentButtons);
        attendancePanel.add(attendancePanelButtons);

        laboratoryNumber.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                laboratoryNumber.setText("");
            }
        });

        studentNumber.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                studentNumber.setText("");
            }
        });

        getByLaboratory.addActionListener(e -> refreshAttendanceTableByLaboratory(Long.parseLong(laboratoryNumber.getText())));

        getByStudent.addActionListener(e -> refreshAttendanceTableByStudent(Long.parseLong(studentNumber.getText())));

        addAttendanceButton.addActionListener(e -> {
            JFrame addAttendanceFrame = new JFrame();
            JPanel addAttendancePanel = new JPanel();
            addAttendanceFrame.setName("Add attendance");
            addAttendanceFrame.setSize(350, 250);
            addAttendanceFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            addAttendanceFrame.setResizable(false);

            JPanel attendanceInfo = new JPanel(new GridLayout(4, 2));
            JLabel studentLabel = new JLabel("StudentID: ");
            JLabel laboratoryLabel = new JLabel("LaboratoryID: ");
            JLabel presenceLabel = new JLabel("Presence: ");

            JTextField studentText = new JTextField(15);
            JTextField laboratoryText = new JTextField(15);
            String[] presence = {"present", "absent"};
            @SuppressWarnings("unchecked") JComboBox presenceComboBox = new JComboBox(presence);

            attendanceInfo.add(studentLabel);
            attendanceInfo.add(studentText);
            attendanceInfo.add(laboratoryLabel);
            attendanceInfo.add(laboratoryText);
            attendanceInfo.add(presenceLabel);
            attendanceInfo.add(presenceComboBox);

            JPanel submitButtonPanel = new JPanel();
            JButton submitButton = new JButton("Submit");
            submitButtonPanel.add(submitButton);
            submitButton.addActionListener(e14 -> {
                int index = presenceComboBox.getSelectedIndex();
                boolean present;
                present = index == 0;
                attendanceResponse.saveAttendance(Long.parseLong(studentText.getText()), Long.parseLong(laboratoryText.getText()), present);
                refreshAttendanceTable();
                addAttendanceFrame.dispose();
            });

            addAttendancePanel.add(attendanceInfo);
            addAttendancePanel.add(submitButtonPanel);
            addAttendanceFrame.add(addAttendancePanel);
            addAttendanceFrame.setVisible(true);
            addAttendanceFrame.setLocationRelativeTo(null);
        });

        editAttendanceButton.addActionListener(e -> {
            JFrame editAttendanceFrame = new JFrame();
            JPanel editAttendancePanel = new JPanel();
            editAttendanceFrame.setName("Edit attendance");
            editAttendanceFrame.setSize(450, 150);
            editAttendanceFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            editAttendanceFrame.setResizable(false);

            Attendance selectedAttendance = attendanceResponse.getAttendanceByID((Long) newAttendanceTable.getValueAt(newAttendanceTable.getSelectedRow(), 0));

            JPanel attendanceInfo = new JPanel(new GridLayout(3, 2));
            JLabel studentLabel = new JLabel("StudentID: ");
            JLabel laboratoryLabel = new JLabel("LaboratoryID: ");
            JLabel presenceLabel = new JLabel("Presence: ");

            JTextField studentText = new JTextField(15);
            JTextField laboratoryText = new JTextField(15);
            String[] presence = {"present", "absent"};
            @SuppressWarnings("unchecked") JComboBox presenceComboBox = new JComboBox(presence);

            //studentText.setText(selectedAttendance.getStudentDTO().getId().toString());
            //laboratoryText.setText(selectedAttendance.getLaboratoryDTO().getId().toString());
            if (selectedAttendance.isPresent())
                presenceComboBox.setSelectedIndex(0);
            else
                presenceComboBox.setSelectedIndex(1);
            attendanceInfo.add(studentLabel);
            attendanceInfo.add(studentText);
            attendanceInfo.add(laboratoryLabel);
            attendanceInfo.add(laboratoryText);
            attendanceInfo.add(presenceLabel);
            attendanceInfo.add(presenceComboBox);

            JPanel submitButtonPanel = new JPanel();
            JButton submitButton = new JButton("Submit");
            submitButtonPanel.add(submitButton);
            submitButton.addActionListener(e15 -> {
                int index = presenceComboBox.getSelectedIndex();
                boolean present;
                present = index == 0;
                attendanceResponse.updateAttendance(selectedAttendance.getId(), Long.parseLong(studentText.getText()), Long.parseLong(laboratoryText.getText()), present);
                refreshAttendanceTable();
                editAttendanceFrame.dispose();
            });

            editAttendancePanel.add(attendanceInfo);
            editAttendancePanel.add(submitButtonPanel);
            editAttendanceFrame.add(editAttendancePanel);
            editAttendanceFrame.setVisible(true);
            editAttendanceFrame.setLocationRelativeTo(null);
        });

        deleteAttendanceButton.addActionListener(e -> {
            attendanceResponse.deleteAttendanceByID((Long) newAttendanceTable.getValueAt(newAttendanceTable.getSelectedRow(), 0));
            refreshAttendanceTable();
        });



        /*
        Assignment panel
         */
        AssignmentResponse assignmentResponse = new AssignmentResponse();
        assignmentPanel = new JPanel();
        assignmentTablePanel = new JPanel();

        JButton addAssignmentButton = new JButton("Add assignment");
        JButton editAssignmentButton = new JButton("Edit assignment");
        JButton deleteAssignmentButton = new JButton("Delete assignment");

        JTable assignmentTable = new JTable();
        assignmentTable.setPreferredSize(new Dimension(600, 300));
        assignmentTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));

        JScrollPane assignmentScrollPane = new JScrollPane(assignmentTable);
        assignmentScrollPane.setPreferredSize(new Dimension(600, 300));

        assignmentTablePanel.add(assignmentScrollPane);
        assignmentPanel.add(assignmentTablePanel);
        assignmentPanel.add(addAssignmentButton);
        assignmentPanel.add(editAssignmentButton);
        assignmentPanel.add(deleteAssignmentButton);

        addAssignmentButton.addActionListener(e -> {
            JFrame addAssignmentFrame = new JFrame();
            JPanel addAssignmentPanel = new JPanel();
            addAssignmentFrame.setName("Add assignment");
            addAssignmentFrame.setSize(450, 130);
            addAssignmentFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            addAssignmentFrame.setResizable(false);

            JLabel titleLabel = new JLabel("Title: ");
            JLabel deadlineLabel = new JLabel("Deadline: ");
            JLabel descriptionLabel = new JLabel("Description: ");
            JLabel laboratoryLabel = new JLabel("LaboratoryNr: ");
            JTextField titleText = new JTextField(15);
            JTextField deadlineText = new JTextField(15);
            JTextField descriptionText = new JTextField(15);
            JTextField laboratoryText = new JTextField(15);

            JPanel assignmentInfoPanel = new JPanel(new GridLayout(4, 2));
            assignmentInfoPanel.add(titleLabel);
            assignmentInfoPanel.add(titleText);
            assignmentInfoPanel.add(deadlineLabel);
            assignmentInfoPanel.add(deadlineText);
            assignmentInfoPanel.add(descriptionLabel);
            assignmentInfoPanel.add(descriptionText);
            assignmentInfoPanel.add(laboratoryLabel);
            assignmentInfoPanel.add(laboratoryText);

            JPanel submitButtonPanel = new JPanel();
            JButton submitButton = new JButton("Submit");
            submitButtonPanel.add(submitButton);
            submitButton.addActionListener(e16 -> {
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
                Date parsedate = null;
                try {
                    parsedate = format.parse(deadlineText.getText());
                } catch (ParseException e2) {
                    e2.printStackTrace();
                }
                java.sql.Date sqlDate = null;
                if (parsedate != null) {
                    sqlDate = new java.sql.Date(parsedate.getTime());
                }
                assignmentResponse.saveAssignment(titleText.getText(), sqlDate, descriptionText.getText(), Long.parseLong(laboratoryText.getText()));
                refreshAssignmentTable();
                addAssignmentFrame.dispose();
            });

            addAssignmentPanel.add(assignmentInfoPanel);
            addAssignmentPanel.add(submitButtonPanel);
            addAssignmentFrame.add(addAssignmentPanel);
            addAssignmentFrame.setVisible(true);
            addAssignmentFrame.setLocationRelativeTo(null);
        });

        editAssignmentButton.addActionListener(e -> {
            JFrame editAssignmentFrame = new JFrame();
            JPanel editAssignmentPanel = new JPanel();
            editAssignmentFrame.setName("Add assignment");
            editAssignmentFrame.setSize(450, 130);
            editAssignmentFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            editAssignmentFrame.setResizable(false);

            JLabel titleLabel = new JLabel("Title: ");
            JLabel deadlineLabel = new JLabel("Deadline: ");
            JLabel descriptionLabel = new JLabel("Description: ");
            JLabel laboratoryLabel = new JLabel("LaboratoryNr: ");
            JTextField titleText = new JTextField(15);
            JTextField deadlineText = new JTextField(15);
            JTextField descriptionText = new JTextField(15);
            JTextField laboratoryText = new JTextField(15);

            Assignment selectedAssignment = assignmentResponse.getAssignmentByID((Long) newAssignmentTable.getValueAt(newAssignmentTable.getSelectedRow(), 0));
            titleText.setText(selectedAssignment.getTitle());
            deadlineText.setText(selectedAssignment.getDeadline().toString());
            descriptionText.setText(selectedAssignment.getDescription());
            laboratoryText.setText(selectedAssignment.getLaboratoryDTO().getId().toString());

            JPanel assignmentInfoPanel = new JPanel(new GridLayout(4, 2));
            assignmentInfoPanel.add(titleLabel);
            assignmentInfoPanel.add(titleText);
            assignmentInfoPanel.add(deadlineLabel);
            assignmentInfoPanel.add(deadlineText);
            assignmentInfoPanel.add(descriptionLabel);
            assignmentInfoPanel.add(descriptionText);
            assignmentInfoPanel.add(laboratoryLabel);
            assignmentInfoPanel.add(laboratoryText);

            JPanel submitButtonPanel = new JPanel();
            JButton submitButton = new JButton("Submit");
            submitButtonPanel.add(submitButton);
            submitButton.addActionListener(e17 -> {
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
                Date parsedate = null;
                try {
                    parsedate = format.parse(deadlineText.getText());
                } catch (ParseException e2) {
                    e2.printStackTrace();
                }
                java.sql.Date sqlDate = null;
                if (parsedate != null) {
                    sqlDate = new java.sql.Date(parsedate.getTime());
                }
                assignmentResponse.updateAssignment(selectedAssignment.getId(), titleText.getText(), sqlDate, descriptionText.getText(), Long.parseLong(laboratoryText.getText()));
                refreshAssignmentTable();
                editAssignmentFrame.dispose();
            });

            editAssignmentPanel.add(assignmentInfoPanel);
            editAssignmentPanel.add(submitButtonPanel);
            editAssignmentFrame.add(editAssignmentPanel);
            editAssignmentFrame.setVisible(true);
            editAssignmentFrame.setLocationRelativeTo(null);
        });

        deleteAssignmentButton.addActionListener(e -> {
            assignmentResponse.deleteAssignmentByID((Long) newAssignmentTable.getValueAt(newAssignmentTable.getSelectedRow(), 0));
            refreshAssignmentTable();
        });



       /*
       Submission panel
        */
        SubmissionResponse submissionResponse = new SubmissionResponse();
        submissionPanel = new JPanel(new GridLayout(0, 1));
        JPanel submissionTablePanels = new JPanel(new GridLayout(0, 2));    //Panel to contain the two tables
        submissionTablePanel = new JPanel();
        assignmentTablePanelInSubmission = new JPanel();

        JButton viewSubmissionsButton = new JButton("View submissions");
        JButton gradeAssignmentButton = new JButton("Grade submission");
        JTextField submissionMark = new JTextField("Enter mark", 15);

        JTable submissionTable = new JTable();
        submissionTable.setPreferredSize(new Dimension(500, 200));
        submissionTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));

        JScrollPane submissionScrollPane = new JScrollPane(submissionTable);
        submissionScrollPane.setPreferredSize(new Dimension(500, 200));

        assignments = new AssignmentTable();

        JTable assignmentTableInSubmission = new JTable();
        assignmentTableInSubmission.setPreferredSize(new Dimension(500, 200));
        assignmentTableInSubmission.setBorder(BorderFactory.createLineBorder(Color.black, 1));

        JScrollPane assignmentScrollPaneInSubmission = new JScrollPane(assignmentTableInSubmission);
        assignmentScrollPaneInSubmission.setPreferredSize(new Dimension(500, 200));
        assignmentTablePanelInSubmission.add(assignmentScrollPaneInSubmission);
        submissionTablePanel.add(submissionScrollPane);
        submissionTablePanels.add(assignmentTablePanelInSubmission);
        submissionTablePanels.add(submissionTablePanel);

        JPanel submissionButtonsPanel = new JPanel();   //Panel for buttons - for nice ui
        submissionButtonsPanel.add(viewSubmissionsButton);

        JPanel submissionGradingPanel = new JPanel();
        submissionGradingPanel.add(submissionMark);
        submissionGradingPanel.add(gradeAssignmentButton);

        JPanel submissionActions = new JPanel(new GridLayout(0, 1));
        submissionActions.add(submissionButtonsPanel);
        submissionActions.add(submissionGradingPanel);
        submissionPanel.add(submissionTablePanels);
        submissionPanel.add(submissionActions);

        viewSubmissionsButton.addActionListener(e -> refreshSubmissionTable((Long) newAssignmentTableInSubmission.getValueAt(newAssignmentTableInSubmission.getSelectedRow(), 0)));
        submissionMark.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                submissionMark.setText("");
            }
        });

        gradeAssignmentButton.addActionListener(e -> {
            submissionResponse.gradeSubmission((Long) newSubmissionTable.getValueAt(newSubmissionTable.getSelectedRow(), 0), Integer.parseInt(submissionMark.getText()));
            refreshSubmissionTable((Long) newAssignmentTableInSubmission.getValueAt(newAssignmentTableInSubmission.getSelectedRow(), 0));
        });



       /*
       Final stuff
        */
        add(finalPanel);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    @SuppressWarnings("Duplicates")
    private void refreshLaboratoryTable() {
        newLaboratoryTable = laboratories.createTableAll();
        newLaboratoryTable.setPreferredSize(new Dimension(600, 500));
        newLaboratoryTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        JScrollPane newPane = new JScrollPane(newLaboratoryTable);
        newPane.setPreferredSize(new Dimension(600, 300));

        laboratoryTablePanel.removeAll();
        laboratoryTablePanel.add(newPane);
        laboratoryTablePanel.revalidate();
        laboratoryTablePanel.repaint();
    }

    @SuppressWarnings("Duplicates")
    private void refreshAssignmentTable() {
        newAssignmentTable = assignments.createAllAssignments();
        newAssignmentTable.setPreferredSize(new Dimension(600, 500));
        newAssignmentTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        JScrollPane newPane = new JScrollPane(newAssignmentTable);
        newPane.setPreferredSize(new Dimension(600, 300));

        assignmentTablePanel.removeAll();
        assignmentTablePanel.add(newPane);
        assignmentTablePanel.revalidate();
        assignmentTablePanel.repaint();
    }

    @SuppressWarnings("Duplicates")
    private void refreshStudentTable() {
        newStudentTable = students.createTable();
        newStudentTable.setPreferredSize(new Dimension(600, 500));
        newStudentTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        JScrollPane newPane = new JScrollPane(newStudentTable);
        newPane.setPreferredSize(new Dimension(600, 300));

        studentTablePanel.removeAll();
        studentTablePanel.add(newPane);
        studentTablePanel.revalidate();
        studentTablePanel.repaint();
    }

    @SuppressWarnings("Duplicates")
    private void refreshAttendanceTable() {
        newAttendanceTable = attendances.createTable();
        newAttendanceTable.setPreferredSize(new Dimension(500, 500));
        newAttendanceTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        JScrollPane newPane = new JScrollPane(newAttendanceTable);
        newPane.setPreferredSize(new Dimension(500, 300));

        attendanceTablePanel.removeAll();
        attendanceTablePanel.add(newPane);
        attendanceTablePanel.revalidate();
        attendanceTablePanel.repaint();
    }

    @SuppressWarnings("Duplicates")
    private void refreshAttendanceTableByLaboratory(Long laboratoryID) {
        newAttendanceTable = attendances.createTableByLaboratory(laboratoryID);
        newAttendanceTable.setPreferredSize(new Dimension(500, 500));
        newAttendanceTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        JScrollPane newPane = new JScrollPane(newAttendanceTable);
        newPane.setPreferredSize(new Dimension(500, 300));

        attendanceTablePanel.removeAll();
        attendanceTablePanel.add(newPane);
        attendanceTablePanel.revalidate();
        attendanceTablePanel.repaint();
    }

    @SuppressWarnings("Duplicates")
    private void refreshAttendanceTableByStudent(Long studentID) {
        newAttendanceTable = attendances.createTableByStudent(studentID);
        newAttendanceTable.setPreferredSize(new Dimension(500, 500));
        newAttendanceTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        JScrollPane newPane = new JScrollPane(newAttendanceTable);
        newPane.setPreferredSize(new Dimension(500, 300));

        attendanceTablePanel.removeAll();
        attendanceTablePanel.add(newPane);
        attendanceTablePanel.revalidate();
        attendanceTablePanel.repaint();
    }

    @SuppressWarnings("Duplicates")
    private void refreshAssignmentTableInSubmission() {
        newAssignmentTableInSubmission = assignments.createAllAssignments();
        newAssignmentTableInSubmission.setPreferredSize(new Dimension(500, 200));
        newAssignmentTableInSubmission.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        JScrollPane newPane = new JScrollPane(newAssignmentTableInSubmission);
        newPane.setPreferredSize(new Dimension(500, 200));

        assignmentTablePanelInSubmission.removeAll();
        assignmentTablePanelInSubmission.add(newPane);
        assignmentTablePanelInSubmission.revalidate();
        assignmentTablePanelInSubmission.repaint();
    }

    @SuppressWarnings("Duplicates")
    private void refreshSubmissionTable(Long assignmentID) {
        SubmissionTable submissions = new SubmissionTable();
        newSubmissionTable = submissions.createSubmissionsByAssignment(assignmentID);
        newSubmissionTable.setPreferredSize(new Dimension(500, 200));
        newSubmissionTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        JScrollPane newPane = new JScrollPane(newSubmissionTable);
        newPane.setPreferredSize(new Dimension(500, 200));

        submissionTablePanel.removeAll();
        submissionTablePanel.add(newPane);
        submissionTablePanel.revalidate();
        submissionTablePanel.repaint();
    }

    private void clearSubmissions() {
        JTable subTable = new JTable();
        subTable.setPreferredSize(new Dimension(500, 200));
        subTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        JScrollPane newPane = new JScrollPane(subTable);
        newPane.setPreferredSize(new Dimension(500, 200));

        submissionTablePanel.removeAll();
        submissionTablePanel.add(newPane);
        submissionTablePanel.revalidate();
        submissionTablePanel.repaint();
    }
}