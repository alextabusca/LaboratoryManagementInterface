package Interface;

import HttpResponse.StudentResponse;
import Interface.util.Encryption;

import javax.swing.*;
import java.awt.*;

class RegisterUI extends JFrame {

    RegisterUI() {

        super("Laboratory Management - Register");
        setName("Login Interface");

        setSize(450, 300);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setResizable(false);

        JPanel userInfoPanel = new JPanel();
        userInfoPanel.setLayout(new GridLayout(7, 2));

        JLabel firstNameLabel = new JLabel("First Name: ");
        JLabel lastNameLabel = new JLabel("Last Name: ");
        JLabel emailLabel = new JLabel("Email: ");
        JLabel passwordLabel = new JLabel("Password: ");
        JLabel tokenLabel = new JLabel("Token: ");
        JLabel groupLabel = new JLabel("Group: ");
        JLabel hobbyLabel = new JLabel("Hobby: ");

        JTextField firstNameText = new JTextField(30);
        JTextField lastNameText = new JTextField(30);
        JTextField emailText = new JTextField(30);
        JPasswordField passwordText = new JPasswordField(30);
        JTextField tokenText = new JTextField(30);
        JTextField groupText = new JTextField(30);
        JTextField hobbyText = new JTextField(30);

        userInfoPanel.add(tokenLabel);
        userInfoPanel.add(tokenText);
        userInfoPanel.add(emailLabel);
        userInfoPanel.add(emailText);
        userInfoPanel.add(passwordLabel);
        userInfoPanel.add(passwordText);
        userInfoPanel.add(firstNameLabel);
        userInfoPanel.add(firstNameText);
        userInfoPanel.add(lastNameLabel);
        userInfoPanel.add(lastNameText);
        userInfoPanel.add(groupLabel);
        userInfoPanel.add(groupText);
        userInfoPanel.add(hobbyLabel);
        userInfoPanel.add(hobbyText);

        JPanel registerLoginPanel = new JPanel();
        JButton registerButton = new JButton("Register");
        registerLoginPanel.add(registerButton);

        registerButton.addActionListener(e -> {
            StudentResponse studentResponse = new StudentResponse();

            //noinspection deprecation
            studentResponse.registerStudent(tokenText.getText(), emailText.getText(), Encryption.passwordEncryption(passwordText.getText()), firstNameText.getText(), lastNameText.getText(), groupText.getText(), hobbyText.getText());

            this.dispose();
        });

        add(userInfoPanel, BorderLayout.CENTER);
        add(registerButton, BorderLayout.SOUTH);

        setLocationRelativeTo(null);
        setVisible(true);
    }

}
